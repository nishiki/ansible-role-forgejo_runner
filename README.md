# Ansible role: Forgejo runner

[![Version](https://img.shields.io/badge/latest_version-0.0.0-green.svg)](https://code.waks.be/nishiki/ansible-role-forgejo_runner/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-forgejo_runner/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-forgejo_runner/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-forgejo_runner/actions?workflow=molecule.yml)

Install and configure Forgejo runner

## Requirements

- Ansible >= 2.10
- Debian
  - Bookworm

## Role variables

| Name                           | Type | Required | Default                                                                 | Comment                    |
| ------------------------------ | ---- | -------- | ----------------------------------------------------------------------- | -------------------------- |
| forgejo_runner_version         | str  | no       | 3.4.1                                                                   | version to install         |
| forgejo_runner_binary_checksum | str  | no       | sha256:5cf1d28e6d2d6168b10f2e7fac5425fe6789491834c047b5f7bb21ab84b9eaf5 | checksum for verify binary |
| forgejo_runner_arch            | str  | no       | amd64                                                                   | architecture of binary     |
| forgejo_runner_name            | str  | no       | inventory_hostname                                                      | runner name in forgejo     |
| forgejo_runner_instance_url    | str  | yes      |                                                                         | forgejo instance url       |
| forgejo_runner_instance_token  | str  | yes      |                                                                         | forgejo token to register  |
| forgejo_runner_config          | dict | no       |                                                                         |                            |

### forgejo_runner_config

All options are in [the documentation](https://forgejo.codeberg.page/docs/latest/admin/actions/#configuration)

Default value:

```yaml
log:
  level: info
runner:
  file: .runner
  capacity: 1
  timeout: 30m
  insecure: false
container:
  force_pull: true
```

## How to use

```yaml
- hosts: server
  roles:
    - forgejo_runner
  vars:
    forgejo_runner_version: 3.4.1
    forgejo_runner_binary_checksum: sha256:5cf1d28e6d2d6168b10f2e7fac5425fe6789491834c047b5f7bb21ab84b9eaf5
    forgejo_runner_arch: amd64
    forgejo_runner_name: mysuperserver
    forgejo_runner_instance_url: https://git.example.com/
    forgejo_runner_instance_token: SECRET_TOKEN
    forgejo_runner_config:
      log:
        level: debug
      runner:
        capacity: 3
      container:
        force_pull: false
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule molecule-docker docker ansible-lint pytest-testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2024 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
