import testinfra.utils.ansible_runner

def test_package(host):
    package = host.package('docker.io')
    assert package.is_installed

def test_user(host):
  user = host.user('forgejo-runner')
  assert user.exists
  assert user.group == 'forgejo-runner'
  assert 'docker' in user.groups
  assert user.home == '/opt/forgejo-runner'
  assert user.shell == '/bin/nologin'

def test_binary_file(host):
  path = host.file('/usr/local/bin/forgejo-runner')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o755

def test_config_file(host):
  path = host.file('/etc/forgejo-runner.yml')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644
  assert path.contains('level: info')

def test_service(host):
  service = host.service('forgejo-runner')
  assert service.is_enabled
